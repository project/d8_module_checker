D8 Module Checker

Checking the availability of the Contributed Module for the Drupal8 manually is tedious. In order to simplify this process, we have developed a module to make it much easier. This module helps you get the version of the contributed Module available in Drupal8.
The report will give the version of the available module and "Not Available" status for the module does not have any contributed Module.

Installation Steps:
1.Download the current module
2.Enable the module as per Drupal procedure
3.Go to "admin>config>system" and click on 
"D8 module check config". Then provide the file path of the Contributed Module location
4.Save the configuration changes
5.Go to "admin>config>system" and click on "D8 module availability report" menu.
6.The report will be generated.

About Author:
Suresh CJ,  Innoppl Technologies Pvt. Ltd